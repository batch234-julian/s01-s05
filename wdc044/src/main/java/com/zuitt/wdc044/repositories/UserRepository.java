package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// this interface handles the CRUD operations
public interface UserRepository extends CrudRepository<User, Object> {
    User findByUsername(String username);
}

