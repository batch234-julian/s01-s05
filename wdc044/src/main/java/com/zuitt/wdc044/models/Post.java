package com.zuitt.wdc044.models;

import javax.persistence.*;
// this comes from the dependency that we have set up in pom.xml
@Entity
// this class is a representation of a database table via this annotation
@Table(name= "posts")
// this annotation designate the table name
public class Post {
    @Id
    // sets up the primary key
        @GeneratedValue
    // auto-increment
    private Long id;
    // id will be a long integer

    @Column
    // class properties will be shown in each column for the table
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    // constructors
        // default constructors for retrieval
    public Post(){}

        // parameterized constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // getters and setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){return user;}

    public void setUser(User user){ this.user = user;}

}









