package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
// generic stereotype
public class JwtUserDetailsService implements UserDetailsService {
    // UserDetailsService- retrieves the user's authentication and authorization information
    @Autowired
        private UserRepository userRepository;

    @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            // UserDetails-store information which later encapsulated into Authentication objects, this allows non-security related user information(email, telephone number, etc) to be stored in a convenient location
            // loadUserByUsername-method from UserDetails, locates the user based on the username
            // throws-> declare the exceptions that can occur during the execution of the program
            // UsernameNotFoundException-thrown when a user cannot be located by its username

            User user = userRepository.findByUsername(username);

            if(user == null){
                throw new UsernameNotFoundException("User not found with username" + username);
            }

            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

}

// visualizations
//            user
//UserDetails-email address, telephone number, gender, password, username
//
//registeredUsers
//user1
//user2
//user3
//
//login- user1
//        UserDetails: -email address1, telephone number1, gender1, password1, username1
//
//        findByUsername(username1)
//
//checking
//        if(user1 == null){
//        throw new UsernameNotFoundException("User not found with username" + username);
//        }
//
//retrieved details        username1
//                         password1
//
//                    ArrayList<>(username1, password1)











